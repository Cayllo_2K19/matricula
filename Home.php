<html>
    <head>
        <title>Cvdd</title>
        <meta charset="utf-8" />
 	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel='icon' href='img/favicon.ico'>
        <link rel="stylesheet" type="text/css" href="Semantic-UI-CSS-master/semantic.min.css">
    </head>
    <body>        
        <?php
            require_once("menu.php");
        ?>
      <div class="pusher">
      <br>
        <?php
            require_once("menuPrincipal.php");
        ?>
        
        <!-- Corpo do Site-->
        <div class='ui main text container'>
        <table class="ui celled table">
            <center><h2>Alunos</h2></center>
            <thead>
                <th>Id</th>
                <th>Nome do Aluno</th>
                <th>E-mail</th>
            </thead>
            <tbody>
                
            <?php
                require_once("confi.php");
                $sql = "SELECT * FROM aluno";
                $result = $conn->query($sql);
                if($result->num_rows){
                    while($row = $result->fetch_assoc()){
                        echo '<tr><td>'.$row['id'].'</td> <td>'.$row['nome_aluno'].'</td> <td>'.$row['email_aluno'].'</td>';
                    }
                }else{
                    echo "<center><h3> Sem resultados...</h3></center>";
                }
            ?>
                
            </tbody>
        </table>
        <table class="ui celled table">
            <center><h2>Cursos</h2></center>
            <thead>
                <th>Id</th>
                <th>Nome do Curso</th>
                <th>Status</th>
            </thead>
            <tbody>
                
                <?php
                    require_once("confi.php");
                    $sql = "SELECT * FROM curso";
                    $result = $conn->query($sql);
                    if($result->num_rows){
                        while($row = $result->fetch_assoc()){
                            echo '<tr><td>'.$row['id'].'</td> <td>'.$row['nome_curso'].'</td> <td>'.$row['status_curso'].'</td>';
                        }
                    }else{
                        echo "<center><h3> Sem resultados...</h3></center>";
                    }
                ?>
                
            </tbody>
        </table>
      </div>
      </div>
    </body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/semantic.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</html>