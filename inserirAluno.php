<html>
    <head>
        <title>Inserir Aluno</title>
        <meta charset="utf-8" />
 	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel='icon' href='img/favicon.ico'>
        <link rel="stylesheet" type="text/css" href="Semantic-UI-CSS-master/semantic.min.css">
    </head>
    <body>
        <?php
            require_once("menu.php");
        ?>
        <div class="pusher">
        <br>
        <?php
            require_once("menuPrincipal.php");
        ?>
        <div class='ui main text container'>
        <br>
        <form method="post" action="inserirAlunoProc.php" class="ui form">
            <div class="ui inverted green segment">
                <div class="ui inverted form">
                    <div class="two fields">
                    <div class="field">
                        <label>Nome:</label>
                        <input placeholder="Digite seu Nome..." type="text" name="aluno">
                    </div>
                    <div class="field">
                        <label>E-mail:</label>
                        <input placeholder="Digite seu E-mail..." type="text" name="email">
                    </div>
                    </div>
                    <button class="fluid ui button" type="submit">Enviar</button>
                </div>
            </div>
        </form>
        </div>
        </div>
    </body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/semantic.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</html>