<html>
    <head>
        <title>Buscar Aluno</title>
        <meta charset="utf-8" />
 	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel='icon' href='img/favicon.ico'>
        <link rel="stylesheet" type="text/css" href="Semantic-UI-CSS-master/semantic.min.css">
    </head>
    <body>        
        <?php
            require_once("menu.php");
        ?>
      <div class="pusher"><br>
        <?php
            require_once("menuPrincipal.php");
        ?>
        
        <!-- Corpo do Site-->
        <form method="post" action="#">
            <div class='ui main text container'>
            <div class="ui fluid icon input">
                <input type="text" placeholder="Procure por um Nome..." name="pesquisa">
                <i class="search icon"></i>
            </div>
        </form>
        <table class="ui celled table">
            <thead>
                <th>Id</th>
                <th>Nome do Aluno</th>
                <th>E-mail</th>
                <th>Apagar</th>
                <th>Editar</th>
            </thead>
            <tbody>
                
                <?php
                    $pesquisa = (isset($_POST["pesquisa"])? $_POST["pesquisa"] : "<center><h3>Sem resultados...</h3></center>" );
                    require_once("confi.php");
                    $sql = "SELECT * FROM aluno WHERE nome_aluno LIKE '%$pesquisa%' || email_aluno LIKE '%$pesquisa%'";
                    $result = $conn->query($sql);
                    if($result->num_rows){
                        while($row = $result->fetch_assoc()){
                            echo '<tr><td>'.$row['id'].'</td> <td>'.$row['nome_aluno'].'</td> <td>'.$row['email_aluno'].'<td><a href="apagarA.php?id='.$row["id"].'"><button  class="ui inverted red button">Apagar</button></a></td><td><a href="editarA.php?id='.$row["id"].'"><button  class="ui inverted green button">Editar</button></a></td></td>';
                        }
                    }else{
                        echo "<center><h3>Sem resultados...</h3></center>";
                    }
                ?>
                
            </tbody>
        </table>
        </div>
      </div>
    </body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/semantic.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</html>